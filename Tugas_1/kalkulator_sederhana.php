<?php
function kalkulatorsederhana(string $input){
    $array = explode(" ",$input);

    if (is_numeric($array[0]) && is_numeric($array[2])){
        if ($array[1] == "+"){
            $hasil = $array[0] + $array[2];
        }
        else if ($array[1] == "-"){
            $hasil = $array[0] - $array[2];
        }
        else if ($array[1] == "*"){
            $hasil = $array[0] * $array[2];
        }
        else if ($array[1] == "/"){
            if ($array[2] == 0){
                throw new  Exception("Tidak dapat dibagi dengan 0\n");
            }
            else{
                $hasil = $array[0] / $array[2];
            }
        }
        else if ($array[1] == "%"){
            $hasil = $array[0] % $array[2];
        }
        else if ($array[1] == "**"){
            $hasil = $array[0] ** $array[2];
        }
        else{
            throw new  Exception("Input Salah operator bilangan tidak terdaftar\n");
        }
    }
    else{
        throw new  Exception("Format input Salah\nBerikut contoh format input yang benar : 2 + 2\n");
    }


    echo "Hasil Operasi ".$hasil."\n";
}

try {
    kalkulatorsederhana(readline("Masukkan Operasi "));
}

catch(Exception $e) {
    echo 'Error: ' .$e->getMessage();
}
