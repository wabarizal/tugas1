<?php
function hitungvokal(string $input){
    $kecil = strtolower($input);
    $array = str_split($kecil);
    $array2 = [];
    $counter = 0;
    if (in_array("a",$array)){
        $counter+=1;
        $array2[] = "a";
    }
    if (in_array("i",$array)){
        $counter+=1;
        $array2[] = "i";
    }
    if (in_array("u",$array)){
        $counter+=1;
        $array2[] = "u";
    }
    if (in_array("e",$array)){
        $counter+=1;
        $array2[] = "e";
    }
    if (in_array("o",$array)){
        $counter+=1;
        $array2[] = "o";
    }
    if ($counter == 0){
        echo "Tidak ditemukan huruf vokal\n";
    }
    else{
        echo $input." = ".$counter." yaitu ".implode(" dan ",$array2)."\n";
    }
}

hitungvokal(readline("Masukkan Kata "));
