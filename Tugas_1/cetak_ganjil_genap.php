<?php
try {
    function cetakganjilgenap(int $batasbawah, int $batasatas) {
        if($batasbawah > $batasatas) {
            throw new Exception("Batas bawah harus lebih kecil atau sama dengan batas atas\n");
        }
        for ($i = $batasbawah; $i <= $batasatas; $i++){
            if ($i % 2 == 0){
                echo "Angka ".$i." adalah genap\n";
            }
            else{
                echo "Angka ".$i." adalah ganjil\n";
            }
        }
    }
    cetakganjilgenap(readline("Masukkan Batas Bawah "), readline("Masukkan Batas Atas "));
}

catch (TypeError $ex) {
    echo "Error : Input Harus Berupa Angka\n";
}

catch(Exception $e) {
    echo 'Error: ' .$e->getMessage();
}